FROM ubuntu:latest
RUN apt-get update && apt-get install -y vim gcc
ADD . .
RUN sh init.sh
WORKDIR /home/mario/acl