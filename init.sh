groupadd waiters
groupadd cooks

useradd -m charlie 
useradd -m cathy
useradd -m willie
useradd -m winona
useradd -m mario

usermod -a -G waiters willie
usermod -a -G waiters winona

usermod -a -G cooks charlie
usermod -a -G cooks cathy

runuser -u mario mkdir /home/mario/acl
runuser -u mario -- gcc -o /home/mario/acl/enqueue enqueue.c
runuser -u mario -- gcc -o /home/mario/acl/dequeue dequeue.c
runuser -u mario -- touch /home/mario/acl/orders.txt
chgrp waiters /home/mario/acl/enqueue
chgrp cooks /home/mario/acl/dequeue
chmod o-x /home/mario/acl/enqueue
chmod o-r /home/mario/acl/enqueue
chmod u+s /home/mario/acl/enqueue
chmod o-x /home/mario/acl/dequeue
chmod o-r /home/mario/acl/dequeue
chmod u+s /home/mario/acl/dequeue