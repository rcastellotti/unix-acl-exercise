This repository contains src for [https://rcastellotti.dev/posts/unix-acl-exercise](https://rcastellotti.dev/posts/unix-acl-exercise/)

To run:

```
docker build -t unix-acl-exercise .
docker run -it unix-acl-exercise bash
```