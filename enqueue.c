#include <stdio.h>
#include <stdlib.h>

int main()
{
  FILE *pFile;

  pFile = fopen("orders.txt", "a");
  if (pFile == NULL)
  {
    perror("Error appending to file.");
    exit;
  }
  fprintf(pFile, "just a simple order\n");
  fclose(pFile);
}